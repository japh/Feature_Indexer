Gherking Languages
------------------

Technically, the gherkin-languages.json is integrated into the gfindex script,
thus allowing autonomous use of gfindex.

This is just a reference copy of gherkin-languages.json
from https://raw.githubusercontent.com/cucumber/cucumber/master/gherkin/gherkin-languages.json

During the make process, an attempt will be made to download the latest version from github.
Once installed, however, the languages will not be updated.

To update the languages in gfindex, simple re-install via 'make install'
