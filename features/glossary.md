Glossary
========

BDD / Behaviour Driven Development

>   A software development methodology which emphasises the collaboration
>   between non-technical stake-holders and technical project members, such as
>   developers and testers.
>
>   The insights gained from discussions between all participants is formalised
>   and recorded in 'feature files' which are intended for all technical and
>   non-technical participants alike.

Feature File

>   A text file which describes a single feature of a project.
>   Feature Indexer only identifies feature files via a '.feature' filename suffix.
>   Feature Indexer expects feature files to contain text conforming to the
>   [Gherkin](https://cucumber.io/docs/gherkin/reference) language syntax
>   and be encoded using UTF-8.

Feature Directory

>   Conventionally, feature files are stored in a single `features/` directory.

Feature Tag

>   A tag which appears before the first gherkin keyword within a feature file.
>   Tags that appear after the first keyword are ignored by Feature Indexer completely.
