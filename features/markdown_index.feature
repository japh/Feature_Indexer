@remove-me
Feature: Generate markdown index for online Git repositories

    Provide an overview of a project's features in a form suitable for
    presentation within an online git repository (as provided by codeberg,
    gitlab, github etc.)

    Git hosters often automatically displays markdown files (.md suffix) as
    static web pages within a project.

    By generating an `readme.md` file for the projects directory, project visitors
    can get a quick idea of the functionality the project is attempting to
    provide.

    Scenario: generate a markdown index of all features in a project's features/ directory.

        Note: 'file modification' below refers to creation, deletion or any
              change to an existing file.

        Given a project using BDD development methods
         When a feature file is modified
         Then an index file should be created in the project's features/ directory
