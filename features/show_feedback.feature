Feature: show feedback

    Unix programs often follow the principle of "no news is good news",
    i.e.: `mv * some_dir/` will only produce output if something goes wrong
    e.g.: "permission denied"

    Feature Indexer *could* also produce no output when an index is
    successfully created, however, Feature Indexer creates files which are
    intended for human consumption and the authors assume that the user wants
    to know which file(s) where created.

    Rule: newly created files should be reported to the user

        Scenario: the name of each created file should be reported to the user

            When an index file is generated
            Then the name of the index file should be displayed

    @low-prio
    Rule: optional statistics

        Some users might be interested in statistics such as the number of
        features that were included in the index.

        However, since the feature parser used by Feature Indexer only reads
        the name and possible description of each feature, the only statistics
        available will be the number of feature files read and perhaps the time
        taken to generate the index.

    @low-prio
    Rule: report unparsable feature files

        If a feature file is not parsable, produce a short 'could not parse' message.

        However, the authors assume that a fully fledged testing system, such
        as those from cucumber.io, will be used to actually process the feature
        files.

        Since these tools define the gherkin language used to write feature
        files, it is their responsibility to produce details error messages.
