Feature: Tag handling within feature files

    Tags, i.e.: @abc can be used to select which features, rules, scenarios or
    examples should be tested.

    As far as parsing is concerned, tags must be 'captured' in advance and then
    'applied' to the next keyword which appears in the feature file.

    This is not guaranteed to succeed, as tags are not allowed on all keywords.
    If tags are found before a non-taggable keyword, they are included in the
    syntax tree as an error (just a special kind of comment).
    In this case, the tags are simply ignored by the test runner.

    Rule: Tags may consist of any non-white-space characters

        Scenario: some weird tags

            When  parsing a feature with the text
                """

                @abc @v-1.2 @!$#@
                Feature: tag test feature

                """
            Then the tag <tag> should <exist?> on <keyword>
                | tag   | exist? |
                | abc   | yes    |
                | v-1.2 | yes    |
                | !$#@  | yes    |
                | wip   | no     |


    Rule: Tags may be spread over multiple lines before a keyword

        @tag1
        some comment
        @tag2
        # a real comment
        @tag3
        # and a commented-out @tag-X
        Scenario: multi-line tags

            When  parsing a feature with the text

                """
                @tag1
                # @tag2 # tags may be commented out
                @tag3

                Feature: a feature with 2 tags
                """

            Then the tag <tag> should <exist?> on <keyword>

                | keyword | tag  | exist? |
                | feature | tag1 | yes    |
                | feature | tag2 | no     |
                | feature | tag3 | yes    |

    Rule: Tags may be applied to the keywords: 'feature', 'rule', 'scenario',
            'scenario outline' and individual 'examples'

        Scenario: multi-line tags

            When  parsing a feature with the text

                """

                @tag1
                @tag2
                Feature: feature1

                    @tag1 @tag3
                    Rule: rule1

                        @tag4
                        Scenario: scenario1

                            @tag5
                            Given given1
                            Then then1

                        @tag5
                        Scenario Outline: scenario2

                            Given <cause>
                            Then <effect>

                            @tag6
                            Examples: examples1
                                | cause  | effect   |
                                | action | reaction |

                            @tag7
                            Examples: examples2
                                | cause   | effect  |
                                | nothing | nothing |

                """

            Then the tag <tag> should <exist?> on <keyword>

                | keyword          | name      | tag  | exist? | notes                     |
                | feature          | feature1  | tag1 | yes    |                           |
                | feature          | feature1  | tag2 | yes    |                           |
                | rule             | rule1     | tag1 | yes    | explicitly set            |
                | rule             | rule1     | tag2 | yes    | inherited from feature1   |
                | rule             | rule1     | tag3 | yes    | explicitly attached       |
                | scenario         | scenario1 | tag1 | yes    | inherited from rule1      |
                | scenario         | scenario1 | tag2 | yes    | inherited from feature1   |
                | scenario         | scenario1 | tag3 | yes    | inherited from rule1      |
                | scenario         | scenario1 | tag4 | yes    | explicitly set            |
                # steps should never have tags
                | step             | given1    | tag1 | no     |                           |
                | step             | given1    | tag2 | no     |                           |
                | step             | given1    | tag3 | no     |                           |
                | step             | given1    | tag4 | no     |                           |
                | step             | given1    | tag5 | no     |                           |
                # scenario outlines are OK
                | scenario outline | scenario2 | tag5 | yes    |                           |
                # multiple examples are OK
                | examples         | examples1 | tag1 | yes    | inherited from rule1      |
                | examples         | examples1 | tag2 | yes    | inherited from feature1   |
                | examples         | examples1 | tag3 | yes    | inherited from rule1      |
                | examples         | examples1 | tag4 | no     | not part of scenario1     |
                | examples         | examples1 | tag5 | yes    | inherited from scenario2  |
                | examples         | examples1 | tag6 | yes    | explicitly set            |
                | examples         | examples1 | tag7 | no     | different set of examples |
                | examples         | examples2 | tag1 | yes    | inherited from rule1      |
                | examples         | examples2 | tag2 | yes    | inherited from feature1   |
                | examples         | examples2 | tag3 | yes    | inherited from rule1      |
                | examples         | examples2 | tag4 | no     | not part of scenario1     |
                | examples         | examples2 | tag5 | yes    | inherited from scenario2  |
                | examples         | examples2 | tag6 | no     | different set of examples |
                | examples         | examples2 | tag7 | yes    | explicitly set            |

