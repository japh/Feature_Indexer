@wip
Feature: Extract the feature name and description from a feature file.

    Indexes generated by Feature Indexer consist entirely of headings
    (derived from directory names), and the name and description of each
    feature file found by 'Find all features in the features/ directory'.

    The Gherkin language has been defined for over 70 human languages.

    It would be *possible* to ensure that each feature file strictly
    adheres to the Gherkin Language Specification in all possible
    localisations.

    However, since Feature Indexer only needs the name and description of each
    feature, it should be sufficient to only parse the absolute minimum
    required to extract this information only.

    It is assumed that *other tools*, such as cucumber, are responsible for
    parsing feature files in detail and reporting any errors within them.

    Rule: comments are ignored

        Scenario: ignore comment lines

            See also the treatment of comments and empty lines when parsing
            descriptions, below.

            Given a feature file
                  """
                  # ignore me
                  Feature: Sample Feature
                  # ignore me too
                  Sample Description
                  # I'm not the comment you are looking for
                  Scenario: here we go
                  """
            When Feature Indexer parses the feature file
            Then the feature name should be "Sample Feature"
             And the feature description should be "Sample Description"

    Rule: The name of a feature follows directly after the first keyword in a feature file.

        Scenario: extract feature name from various keyword variants

            I know, 1-line feature files should not exist, but we're only testing
            the extraction of the feature's name from a line consisting of a
            keyword, a ':' and some text which will be used as the name.

            Note that the name will be capitalised in order to be more pleasing to
            the eye.

            Given a 1-line feature file
              And containing the first line <line>
             When Feature Indexer parses the feature file
             Then the feature name should be <name>

            @positive-test
            # Note: indentation between the keyword and name is also removed
            Examples: typical feature lines
                  | Line                                | Name                 | Comments                                                   |
                  | ----                                | ----                 | --------                                                   |
                  | Feature:       you want this        | You want this        | standard line                                              |
                  | Business Need: you need this        | You need this        | alternative keyword, including spaces                      |
                  | Abstract:      does this look good? | Does this look good? | not actuallly a keyword, but the right form                |
                  | โครงหลัก:       how about Thai?      | How about Thai?      | valid Thai localisation                                    |
                  | Pretty much:   whatever ya reckon   | Whatever ya reckon   | valid en-au keyword, apparently                            |
                  | foo bar:       sample two           | Sample two           | a lower-case <keyword>: <name> pair, with a 2-part keyword |
                  | foo bar baz:   sample three         | Sample three         | a lower-case <keyword>: <name> pair, with a 2-part keyword |

            @negative-test
            Examples: non-feature lines
                  | Line         | Name | Comment                                                      |
                  | ----         | ---- | -------                                                      |
                  |              |      | no text                                                      |
                  | Feature      |      | only 1 token - need to match the '<keyword>: <name>' pattern |
                  | Feature 1    |      | no : after the keyword                                       |
                  | #language az |      | a comment - continue parsing for a feature                   |

            @negative-test
            Examples: all '<keyword>: <name>' pairs are parsed in the same way
                  | Line                          | Name         | Comment                                                    |
                  | ----                          | ----         | -------                                                    |
                  | Note:                         |              | Lines ending in : treated as text                          |
                  | : a strange bullet point      |              | empty keywords are treated as text                         |
                  | http://example.com            |              | there must be a space between the : and the name           |
                  | - bullet point: remember this |              | all keywords must consist of [[:alnum:]] characters only   |
                  | 'see': just saying...         |              | all keywords must consist of [[:alnum:]] characters only   |

    Rule: A feature's description is all non-comment text lines between the first two section keywords of a feature file.

        Now we get to the meat of the parsing process, all of the following
        samples should produce the same result.

        A 'section keyword' is any keyword that is followed by a ':', and thus
        starts a new 'block' or 'section'.

            -   All comment lines are treated like empty lines (they act as paragraph separators)

            -   Tag lines are parsed silently (not handled by this feature)

            -   The first '<keyword>: <name>' pair are always assumed to be the feature name

            -   All following lines are assumed to be the feature's description

                Whereby:
                    -   all leading and trailing empty lines are removed entirely
                    -   the minimal, common amount of indentation is removed from
                        all lines in the description
                    -   any groups of empty lines (after comment replacement)
                        are 'squashed' to a single empty line

            -   The second '<keyword>: <name>' pair is expected to be a 'Rule:',
                'Background:' or 'Scenario:', thus ending the description

        Scenario: various feature styles, but all containing the same feature name and description

            We cannot really use an Examples table here, since each feature file
            has more text than can be squashed into a one-line cell of a table.

            Given one of the feature files

                  # minimal sample
                  """
                  Feature: sample 1
                    a description.

                    over two lines.
                    Scenario: first things first
                    This is the scenario description - not included in the feature
                  """

                  # extra empty lines are 'squashed'
                  """
                  Feature: sample 1



                    a description.




                    over two lines.



                    Scenario: first things first
                  """

                  # a random language and lots of comments,
                  # which are treated like empty lines (paragraph separators)
                  """
                  # language: no
                  # extra comments are always good
                  Egenskap: sample 1
                      # with a couple of
                      # comment lines before...
                      a description.
                      # and even multiple comment lines
                      # between...
                      over two lines.
                      # and even a comment or two
                      # after the two lines of description
                      Bakgrunn: I have never learnt Norwegian!
                  """

                  # tags are parsed but not part of the 'feature name and description' pattern
                  """

                  # language: ru

                  @tags @are @great
                  Функциональность: Sample 1

                  # I really hope that I'm not swearing in russion here!
                  # you never can tell, and like all humans, they can easily take things the wrong way

                  A description.



                  Over two lines.


                  Правило: this is apparently a rule
                  """

             When Feature Indexer parses the feature file
             Then the feature name should be "Sample 1"
              And the feature description should be
                  """
                  A description.

                  Over two lines.
                  """
