#language:en

@remove
# Note: this feature should be moved to a different parser
# The parser for Feature Indexer only need to parse generic <keyword>: <name>
# pairs, tags, comments and the text between the first two keywords of any
# feature file.

@wip @untested

Feature: Feature files may use any of the available locales

    The Gherkin language allows feature files to be written in many native languages.

    The translation is performed using the mappings defined in
    gherkin-languages.json which is part of the official gherkin parser code,
    publically available at https://github.com/cucumber/cucumber/blob/master/gherkin/gherkin-languages.json

    By default, the 'en' english locale is assumed.

    Projects may specify their preferred locale for all of their features (parser specific).

    The first line of a feature file may specify which language the file is
    written in, via the text: '# language: ...'

    Rule: The default locale is assumed to be 'en'

        If no default locale has been specified, 'en' should be assumed for
        feature files which don't begin with '# language:'.

        @positive-test
        Scenario Outline: Assume 'en' if no locales are specified (default or via '# language')

            Given the default locale has not been set
            Whenever should not match
            When    parsing a feature with the text
                    """
                    Feature: using fallback default 'en' locale
                    """
            Then parsing should succeed
            And  the feature should be called "using fallback default 'en' locale"

    Rule: The default locale may be set explicitly

        If a default locale has been specified, then it should be assumed for
        feature files which don't begin with '# language:'

        @positive-test
        Scenario: Assume explicitly defined default locale

            Given the default locale has been set to 'de'
            When  parsing a feature with the text
                    """
                    Funktionalität: using explicit default 'de' locale
                    """
            Then parsing should succeed
            And  the feature should be called "using explicit default 'de' locale"

    Rule: Each feature file may specify its own locale by starting with a '# language' line.

        @positive-test
        Scenario: Parse features using the locale specified via '# language:'

            Given the default locale has not been set
            When parsing a feature with the text
                    """
                    # language: de
                    Funktionalität: using explicit 'de' locale
                    """
            Then parsing should succeed
            And the feature should be called "using explicit 'de' locale"

        @positive-test
        Scenario: Allow 'en' locale over default 'de' locale

            Given the default locale has been set to 'de'
            When  parsing a feature with the text
                    """
                    # language: en
                    Feature: using explicit 'en' locale over default 'de' locale
                    """
            Then parsing should succeed
            And  the feature should be called "using explicit 'en' locale over default 'de' locale"

    Rule: Keywords which do not match the locale (default or explicit) are treated as 'descriptive text'

        @positive-test
        Scenario: Only inteperet one language within a feature file - english

            When parsing a feature with the text
                    """
                    # language: en
                    Feature:        one feature in two languages
                    Funktionalität: eine Funktion in zwei Sprachen
                    """
            Then parsing should succeed
             And the feature should be called "one feature in two languages"
             And the "Funktionalität:" line should be treated as descriptive text
             And the descriptive text of the feature should be "Funktionalität: eine Funktion in zwei Sprachen"

        Scenario: Only inteperet one language within a feature file - german

            When parsing a feature with the text
                    """
                    # language: de
                    Feature:        one feature in two languages
                    Funktionalität: eine Funktion in zwei Sprachen
                    """
            Then parsing should succeed
             And the "Feature:" line should be ignored
             And the feature should be called "eine Funktion in zwei Sprachen"
             And the feature should not have any descriptive text

        @negative-test
        Scenario: German keywords should not be valid without '# language: de'

            Given the default locale has not been set
            When parsing a feature with the text
                    """
                    Funktionalität: attempt to german with no default language specified
                    """
            Then parsing should fail
            # test for known known error message? "no Feature: found" - all unknown keywords are just 'text'

        @negative-test
        Scenario: English keywords should not be valid without '# language: en'

            Given the default locale has been set to 'de'
            When parsing a feature with the text
                    """
                    Feature: attempt to use english with a german default
                    """
            Then parsing should fail
            # test for known known error message? "no Feature: found"

    Rule: All known keywords must be parsed properly

        Scenario: full keyword test in english
            When parsing a feature with the text

                """
                Feature: Testing original keywords
                Background: some steps to be run before each following scenario
                Rule: a business rule definition
                Scenario:   a simple scenario
                    Given some initial state
                    And some initial configuration
                    When something happens
                    But nothing goes wrong
                    Then some results should be checked
                     * like this
                     * and this
                Scenario Outline:
                    Examples:
                        # insert table here
                """

                """
                Ability: An alternative to "Feature:"
                Example: Alternative to Scenario:
                Scenario Template:
                    Scenarios:
                        # insert table here
                """

                """
                Business Need: Yet another alternative to "Feature:"
                """
            Then parsing should succeed
            # and we need a way to test the results
