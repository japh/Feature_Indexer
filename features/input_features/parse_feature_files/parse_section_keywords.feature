# language: de

@remove
# Note: this feature should be moved to a different parser
# The parser for Feature Indexer only needs to parse generic <keyword>: <name>
# pairs, tags, comments and the text between the first two keywords of any
# feature file.

@untested 
Funktionalität: Parse section keywords

    The Gherkin language specifies two kinds of keywords:

        Keywords with a trailing ':' for sections.
        Keywords without a trailing ':' for steps.

    This feature only tests the behaviour of the parser for section keywords
    and their allowed structures,

    Rule: the Feature: keyword must be the first section and must appear only once per feature file

    @positive-test
    Szenario: Feature as first section

        Wenn parsing a feature with the text
             """
             Feature: this is a valid feature
             """
        Dann parsing should succeed
        Und  the feature's name should be "this is a valid feature"

    @negative-test
    Szenario: two Feature: keywords

        Wenn parsing a feature with the text
             """
             Feature: this would be a valid feature
             Feature: but this keyword is not allowed
             """
        Dann parsing should fail
        # test for known known error message?
