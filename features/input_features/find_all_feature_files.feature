@prepared
Feature: Find all feature files in a features/ directory

    Feature Indexer generates an index of all feature files found within a
    project's 'features/' directory.

    Feature files must have a '.feature' filename suffix.

    Any additional, unrecognised files are ignored.

    Rule: Only files with a '.feature' filename suffix should be included in the index

        Scenario: A features/ directory containing a mix of files with various filename suffixes

            Given a features/ directory containing the files
                  | guidelines.pdf  |
                  | glossary.md     |
                  | logo.png        |
                  | sample1.feature |
                  | sample2.feature |
                  | sample3.feature |

             When Feature Indexer scans the features/ directory

             Then 3 feature files should be found
                  | sample1.feature |
                  | sample2.feature |
                  | sample3.feature |

    Rule: All sub-directories must be traversed recursively

        Scenario: The features/ directory contains a deep directory structure

            Given a features/ directory containing the files and directories
                  | sample1.feature                          |
                  | sub1/sample2.feature                     |
                  | sub1/sub2/sample3.feature                |
                  | sub1/sub2/sub3/sub4/sub5/sample4.feature |

             When Feature Indexer scans the features/ directory

             Then 4 feature files should be found
                  | sample1.feature |
                  | sample2.feature |
                  | sample3.feature |
                  | sample4.feature |

    Rule: Empty directories must be ignored

        An 'empty directory' is any directory which, recursively, contains no feature files.
        i.e.: there are no feature files in that directory or any of its sub-directories.

        A directory only containing other files, or further empty directories is, for
        the purpose of index generation, still considered 'empty'.

        Scenario: Empty leaf directories

            Given a features/ directory containing the files and directories
                  | Path                            | Notes            |
                  | ----                            | -----            |
                  | distraction1.txt                | non-feature file |
                  | sample1.feature                 |                  |
                  | sub1/                           |                  |
                  | sub1/distraction2.txt           | non-feature file |
                  | sub1/sample2.feature            |                  |
                  | sub1/sub2/                      | no feature files |
                  | sub1/sub2/distraction3.txt      | non-feature file |
                  | sub1/sub2/sub3/                 | no feature files |
                  | sub1/sub2/sub3/distraction4.txt | non-feature file |

              But no feature files in the directory "sub1/sub2/"
              And no feature files in the directory "sub1/sub2/sub3/"

             When Feature Indexer scans the features/ directory

             Then 2 feature files should be found
                  | sample1.feature |
                  | sample2.feature |

              And 1 directory should be included
                  | Directory      | Include? | Heading | Reason                                              |
                  | ---------      | -------- | ------- | ------                                              |
                  | sub1           | yes      | Sub1    | sub1/sample2.feature                                |
                  | sub1/sub2/sub3 | no       |         | .../sub3 contains no feature files                  |
                  | sub1/sub2      | no       |         | only contains sub3, which contains no feature files |

        Scenario: Empty intermediate directories

            Intermediate directorecties contain no feature files themselves,
            but one of their sub-directory trees contains at least one feature
            file.

            To avoid clutter, only the directories which directly contain
            feature files are included as headings in the index.

            Intermediate directories are not included in the index.

            Given a features/ directory containing the files and directories
                  | distraction1.txt                          |
                  | sample1.feature                           |
                  | sub1/                                     |
                  | sub1/distraction2.txt                     |
                  | sub1/sample2.feature                      |
                  | sub1/sub2/                                |
                  | sub1/sub2/distraction3.txt                |
                  | sub1/sub2/sample3.feature                 |
                  | sub1/sub2/sub3/                           |
                  | sub1/sub2/sub3/distraction4.txt           |
                  | sub1/sub2/sub3/sub4/                      |
                  | sub1/sub2/sub3/sub4/distraction5.txt      |
                  | sub1/sub2/sub3/sub4/sub5/                 |
                  | sub1/sub2/sub3/sub4/sub5/distraction6.txt |
                  | sub1/sub2/sub3/sub4/sub5/sample4.feature  |

              But no feature files in the directory "sub1/sub2/sub3/"
              And no feature files in the directory "sub1/sub2/sub3/sub4/"

             When Feature Indexer scans the features/ directory

             Then 3 directories should be included
                  | Directory                | include? | Heading | Reason                             |
                  | ---------                | -------- | ------- | ------                             |
                  | sub1                     | yes      | Sub1    | sample2.feature                    |
                  | sub1/sub2                | yes      | Sub2    | sample3.feature                    |
                  | sub1/sub2/sub3           | no       |         | .../sub3 contains no feature files |
                  | sub1/sub2/sub3/sub4      | no       |         | .../sub4 contains no feature files |
                  | sub1/sub2/sub3/sub4/sub5 | yes      | Sub5    | sample4.feature                    |

              And 4 feature files should be found
                  | sample1.feature |
                  | sample2.feature |
                  | sample3.feature |
                  | sample4.feature |

