@smoke_test
Feature: create feature index

  The Feature Indexer should look for all Gherkin feature files from a given
  directory, read their respective name and description, and generate in index
  of all feature names with their descriptions so that anyone can get an
  overview of the project's features.
