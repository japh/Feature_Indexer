Feature: write markdown index

  Some providers for git hosting services like Github or Codeberg render
  markdown files automatically. Therefore Feature Indexer is supporting markdown
  as output format so that visitors of the remote repositories see a formatted
  version of the feature index .

  Note that github and codeberg (at least) automatically render 'readme.md'
  files, but not 'index.md' when displaying the contents of a directory.

  The basic index structure is constructed from 3 components:

    - directory names
    - feature names
    - feature descriptions

  Additionally, tags that appear before each feature's 'Feature:'
  keyword (or equivalent)) may be used to restrict

  No other feature contents are used during the construction of an
  index.

  Rule: use header style according to subdir level of the feature file

    Scenario: use top level heading on subdirs under feature root

  @future
  Rule: add breadcrumb navigation to headline

    # sub2
    [features/sub1/sub2](features/sub1/sub2)

  Rule: features should appear as list directly under their directory headlines

  @optional
  Rule: link headlines to feature directories via relative links


  Rule: link feature names to feature files by relative links


  Rule: add description and feature name as plain text


  Rule: use header style according to subdir level of the feature file

          for subdirs deeper than 6 levels beneath the feature dir
          use h6 aka "#######"

    Q: what about 'directories that only contain directories'?
    i.e.:
        sub1/
            f1.feature
            sub2/
                sub3/
                    f2.feature

        sub2 only contains sub3
            - should we get

                # sub1
                - f1
                ## sub2
                ### sub3
                - f2

            - or just

                # sub1
                - f1
                ## sub3
                - f2

            ?

    Scenario: features in subdirs
      Given a feature named "sample"
        And the features location is <location> in the feature dir
       When the feature index is written as markdown
       Then the headline above the the sample feature is "## sub1"

    Examples: subdirs
      | location                                            | headline      |
      | "sub1/sample.feature"                               | "## sub1"     |
      | "sub1/sub2/sample.feature"                          | "### sub2"    |
      | "sub1/sub2/sub3/sample.feature"                     | "#### sub3"   |
      | "sub1/sub2/sub3/sub4/sub5/sub6/sub7/sample.feature" | "###### sub7" |

    Scenario: top level feature
      Given a feature named "sample"
        And the features location is in the feature dir
        And the feature dir is named "features"
       When the feature index is written as markdown
       Then the headline above the the sample feature is "# features"
        And the headline is the first line of the index


  Rule:  a headline should be followed by the path relative to the fature dir in italic

    Scenario: show path after headline
      Given a feature named "sample"
        And the features location is "/sub1/sub2/sub3/sample.feature" in the feature dir
       When the feature index is written as markdown
       Then the headline above the the sample feature is "### sub3"
        And the line beneath the headline is "*sub1/sub2/sub3*"


  Rule: add feature names as list under headlines and link feature names to feature files by relative links

    Scenario: simple feature
      Given a feature named "sample"
        And the features location is "sub1/sub2/sub3/sample.feature" in the feature dir
       When the feature index is written as markdown
       Then the feature is added as "* [sample](sub1/sub2/sub3/sample.feature)" to the index

    Scenario: feature names with special chars
      Given a feature named <feature_name>
        And the features location is "sub1/sub2/sub3/sample.feature" in the feature dir
       When the feature index is written as markdown
       Then the feature is added as md_output to the index

    Examples: special chars
      | feature_name       | md_output                                              |
      | "- minus sample"   | "- [\- minus sample](sub1/sub2/sub3/sample.feature)"   |
      | "# hash sample"    | "- [\# hash sample](sub1/sub2/sub3/sample.feature)"    |
      | "1. number sample" | "- [1\. number sample](sub1/sub2/sub3/sample.feature)" |


  Rule: add description as plain text beneath feature name

    Scenario: description available
      Given a feature named "sample"
        And the feature has a description "description of sample"
       When the feature index is written as markdown
       Then the feature is added as "* sample" to the index
        And the following line is "  sample description"

    Scenario: description with leading whitespace
      Given a feature named "sample"
        And the feature has a description "    description of sample"
       When the feature index is written as markdown
       Then the feature is added as "* sample" to the index
        And the following line is "  sample description"

    Scenario: no description available
      Given a feature named "sample"
        And the feature has no description
        And another feature "sample2" in the same directory
       When the feature index is written as markdown
       Then the feature is added as "* sample" to the index
        And the following line is "  *no description*"
        And the following line is "* sample2"

  @low-prio
  Rule: put headlines for folders under features with the same name

        if a feature file has the same name as its directory the it should appear
          on top of the list of all features of the same directory in the index markdown

    e.g.: features/formatting/formatting.feature

        features/
          + formatting.feature      <--- should appear first in the "formatting" group because it has the same name as the directory
          | formatting/
          | ===========
          +---> Output Formatting
                About Output Formats (about.feature)
                Output Styles

        formatting.feature
            ---
            Feature: Output Formatting
                ...
            ---

    Scenario: sub directory with matching feature

        Given a features directory with the structure

            | features/first.feature         |
            | features/sample.feature        |
            | features/sample/               |
            | features/sample/second.feature |
            | features/sample/third.feature  |

        And every feature has a name and a description "..."

       When the feature index is written as markdown
       Then the resulting index should have the structure

            | content  | line type   |
            | Features | heading     |
            | First    | feature     |
            | ...      | description |
            | Sample   | heading     |
            | ...      | description |
            | Second   | feature     |
            | ...      | description |
            | Third    | feature     |
            | ...      | description |
