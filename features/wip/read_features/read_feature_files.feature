Feature: read feature files

  Every feature file is read to extract the name and description of each feature
  so that index can be generated using this information.

  Rule: the feature name is all the remaining line after a valid "Feature:" keyword

    # add examples for
    #   alternative keywords
    #   interantional keywords?
    #   feature files with syntax errors
    #   features with preceding tags

  Rule: description is every line following the feature specification line



  Rule: comments are ignored


  # tbd could be solved by formatting results
  Rule: leading and trailing lines with withespaces are ignored for descriptions
