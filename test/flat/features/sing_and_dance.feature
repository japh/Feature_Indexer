Feature:    this thing should sing and dance

    As a control freak
    I want it to sing and dance for me.

    That's all

    Scenario: Dance for me

        Given I am feeling bored
        When I tell it to sing and dance
        Then it should sing and dance
        But it should not ask my why
