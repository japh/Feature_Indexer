Ability: Sing children's songs

    As a simple minded person
    I want to hear children's songs all day long

    Scenario: sing a childrens song
        Given I am feeling bored
        When I ask it to sing a children's song
        Then it should sing "Mary had a little lamb"
