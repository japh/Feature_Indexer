Feature: introduction

    This feature should appear first.

    Note that that the `first_steps/` directory appears `alphabeticaly` before
    `introduction`, but it should only appear AFTER all of the files in the
    same directory.

    Rule: show files, then directories
