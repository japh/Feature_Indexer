#language: unknown
@lots @of @tags
Business Need: install for the first time

        As the poor guy who has to get this thing running
        I want to install it quickly
        So that my pointy haired boss doesn't hit me

        By the way:

            did you notice the colon at the end of the previous line?
            Since there's nothing following it, it will NOT get parsed as a keyword.

            : also, a line that starts with a colon is also not a keyword

            but a colon in the middle of a line will of course be interpereted
            as a keyword line, thus closing off this block, and cutting off any
            following conents.

            Indenting is also interesting
                -   the outmost indented line (e.g. the last line in this example)
                    defines the indentation level for the whole text block

            # comments like this one will be discarded completely
            # even if they contain a : in them somewhere

    So, this should be the last line of this file's info.

    #
    # and the next line starts the guts of this feature, which isn't being used
    # in the index, so everything below this line will simply be ignored
    # completely.
    #
    # Knock yourself out, whack as many errors down there as you like! :-)
    #
    Rule: don't get hit

        It can be quite embarassing when the boss hit's me in front of
        everyone.

        It's like:

            Do you even know what you're doing?

        And I'm like:

            Of course, but it wasn't MY fault that your nephew spilt his coffee
            over the power supply!

        Background: assume that all hardware exists and is working

            Given an available server
            And enough RAM
            And a really quick network
            But no stupid users
