#language: unknown
@lots @of @tags
Business Need: install for the second time

    If it's worth doing
    do it again and again and again...

    Rule: wash, rinse, dry, repeat

        Scenario: just because you can do something, does not mean you should

            Given an available server
            And it has been installed
            When asked to re-install it
            Then delete everything
            And start from the beginning
