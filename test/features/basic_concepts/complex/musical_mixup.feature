Ability: Mix up a song and dance

    As a psychodelic audiopath
    I want it to sing a complex song while dancing a different complex dance

    Scenario: Silly mix up
        Given a weird state of mind
        When I ask it to mix up its song and dance
        Then it should sing "Bohemian Rhapsody"
        And dance "Tango Argentino"
