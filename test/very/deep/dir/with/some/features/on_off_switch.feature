Business Need: On / Off Switch

    Every system needs an on/off switch.

    As an administrator
    I want to be able to turn it on at some point after power has been applied
    And I want to turn it off without having to violently pull the plug.

    Scenario: connect power
        Given a system
        When power is connected
        Then it should not start up immediately

    Scenario: turn on after power connection
        Given a system
        And power is connected
        When it is turned on
        Then it should start up immediately

    Scenario: turn off while power is still connected
        Given a system
        And power is connected
        And it is turned on
        When it is turned off
        Then if should stop working
