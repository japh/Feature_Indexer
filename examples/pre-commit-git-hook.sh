#!/bin/sh

#
# update and include the features index as part of the commit process,
# but only if the index is missing or *.feature files have changed.
#

# (example) Installation:
#
#   cd <your project's git workspace>
#   cp <gherkin_feature_indexer>/examples/pre-commit-git-hook.sh support/
#   ln -s ../../support/pre-commit-git-hook.sh .git/hooks/pre-commit
#

# Configuration:
#
# Note: all paths are relative to the top of this git workspace
#

# the name of your Gherkin features/ directory
feature_dir="features/"
feature_index="${feature_dir}ReadMe.md"

# path to gfindex relative to your workspace root
gfindex="./gfindex"

# ---------------------
# No changes below here
#

if [[ -z $gfindex ]]
then
    echo "Please configure the path to gfindex"
    exit 1
fi

if [[ ! -x $gfindex ]]
then
    echo "Gherkin indexer '${gfindex}' is not executable"
    exit 1
fi

if [[ ! -d $feature_dir ]]
then
    echo "Missing feature dir: ${feature_dir}"
    exit 1
fi

index_exists=$( test -f "${feature_index}" )
changed_features=$( git status --porcelain "${feature_dir}**.feature" | wc -l | tr -d ' ' )

if [[ $index_exists != 0 ]]
then
    echo "Creating $feature_index"
    generate=1
elif [[ $changed_features != 0 ]]
then
    echo "Updating $feature_index"
    generate=1
fi

if [[ $generate != 0 ]]
then
    "${gfindex}" --index "${feature_index}"
    git add "${feature_index}"
fi

exit
