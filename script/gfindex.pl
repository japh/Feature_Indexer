#!/usr/bin/perl

#
# This script generates indexes for directories of BDD feature definitions.
#

use v5.18;
use strict;
use warnings;

use Getopt::Long qw( GetOptions );  # core module since perl v5
use Pod::Usage   qw( pod2usage  );  # core module since perl v5.6
use File::Find   qw( find       );  # core module since perl v5

# Note: don't forget to also update the VERSION POD section above
our $VERSION = "v0.0.4";

#
# Currently support index file formats
#
my $format = {
             # Note: each format must define:
             #
             #   getopt:    The getopt option definition, without the :s (optional string) suffix
             #              Aliases for the option may be separated by '|'
             #              (the first option defines the key used in the opt hash)
             #
             #   suffix:    A '|' separated list of possible file name suffixes for the format.
             #              The list is used as a pattern when auto-detecting the
             #              file format from the --index filename.
             #              The first suffix will be used by default when writing files.
             #
             #   generator: The full name of the generator package for the format.
             #              The package MUST provide a new() method for
             #              creating the generator, and a render() method which takes a
             #              feature_tree and returns the text to be written to a file.
             #              TODO: correctly adjust relative links to feature
             #                    files for various --dir and --index combinations
             #              Note: Generators are NOT responsible for writing to the file system!
             #                    Reasons:
             #                      - improved testability.
             #                      - the ability to 'stream' output, if the need should arise.
             html     => { getopt => 'html',        suffix => 'html|htm',    generator => 'generator::html'     },
             json     => { getopt => 'json',        suffix => 'json',        generator => 'generator::json'     },
             markdown => { getopt => 'markdown|md', suffix => 'md|markdown', generator => 'generator::markdown' },
             yaml     => { getopt => 'yaml|yml',    suffix => 'yaml|yml',    generator => 'generator::yaml'     },
             };

my $help =  {
            error   => { -verbose =>  1, -exitval => 1 },
            usage   => { -verbose =>  1, -exitval => 0 },
            full    => { -verbose =>  2, -exitval => 0 },
            version => { -verbose => 99, -exitval => 0, -sections => [ qw( VERSION ) ] },
            };

# "return 1 if caller;" - trick to help testing:
#   Only *run* the main code if this script was called directly from the
#   command line.
#   When this script is loaded by the test/*.t files, caller will be 'true' and
#   thus we'll just be left with a buch of subs and packages and return true to
#   indicate that all is well.
return 1 if caller;

=head1 NAME

index_gherkin - create an index of BDD / Gherkin files

=head1 VERSION

This document describes Feature Indexer version v0.0.4

=head1 SYNOPSIS

index_gherkin [options]

=head1 DESCRIPTION

C<index_gherkin> generates an index for any directory containing I<feature
files>, written in L<Gherkin|https://cucumber.io/docs/gherkin/reference>,
which are typically used when using Behaviour Driven Development (B<BDD>)
practices.

For more information about BDD see L<https://cucumber.io> or your favourite
search engine.

=head2 Index Generation Process

=over

=item 1.

Find all F<*.feature> files in the F<features/> directory.

=item 2.

Parse each feature file:

=over

=item Header

Skip all lines until the first C<I<{keyword}>: I<{name}>> line is found.

=item Feature Name

Assumed that the first C<I<{keyword}>: I<{name}>> line is the C<Feature:> (or
equivalent) followed by the name of the feature.

=item Feature Description

Any text between the first keyword line and the second keyword line (assumed to
be I<Rule:>, I<Scenario:> or I<Background:> or their respective equivalents) is
captured as the feature's description.

Note: comments and tags are also ignored within a feature's description.

=item Trailer

Skip everything after the second C<I<{keyword}>: I<{name}>> line.

=back

=item 3.

Generate one or more index files in the specified format(s).

=back

=head1 OPTIONS

=head2 Help Options:

=over

=item B<--help>

Display this help and exit.

=item B<--usage>

Display a short overview of the options and exit.

=item B<--version>

Display the version and exit.

=back

=head2 Input Options:

=over

=item B<--dir> F<{dir}>

The F<features/> directory to be indexed

Default: F<features/>

=back

=head2 Output Options:

=over

=item B<--index> F<{file}>

The (base)name of the index file(s) to generate.

Default: F<{dir/}index>

If the C<--index> option has a suffix which matches one of the known formats,
then the appropriate format will be automatically selected.

If C<--dir> was specified more than once, then the index file will be written
in the current directory, otherwise it will be written in the directory
specified by C<--dir>.

=item B<--html> F<{file}>

The name of the html index file to generate

Default: F<{index}.html>

=item B<--markdown> F<{file}>

The name of the markdown index file to generate

Default: F<{index}.md>

=item B<--md> F<{file}>

See B<--markdown>

=back

=head2 Defaults:

No index is generated by default (!)

Either the filename specified by C<--index> must have a known format suffix,
or at least one of the format-specific options must be provided.

=head1 EXAMPLES:

S<% index_gherkin B<--md>>

Scan the F<features/> directory for F<*.feature> files and create a
F<features/index.md> markdown file.

S<% index_gherkin --index test/feature_mapB<.md>>

Scan the F<features/> directory for F<.feature> files and create a
F<test/feature_map.md> markdown file.

S<% index_gherkin --dir test/features --index test/feature_map B<--html> B<--md> test/markdown.md>

Scan the F<features/> directory for F<*.feature> files and create a
F<test/feature_map.html> and a F<test/markdown.md> file.

=head1 AUTHOR

Stephen Riehm C<< <japh-codeberg@opensauce.de> >>

=head1 COPYRIGHT AND LICENSE

Copyright 2020-2021, Stephen Riehm. All rights reserved.

This progam is free software;
you can redistribute it and/or modify it under the same terms as Perl itself.
See L<perlartistic>.

=cut

#-----------------------------------------------------------------------
#
# MAIN
#
my $opt = {};
GetOptions( $opt,
            # help
            'help',             # show full help and exit
            'usage',            # show usage and exit
            'version',          # show version and exit
            # runtime configuration
            'verbose|v',        # turn on verbose output
            'dir=s',            # directory to scan for feature files
            'index=s',          # (base)name of the index file(s) to generate
            # dynamically add options for each known format, e.g.: --html, --markdown etc
            map { "$format->{$_}{getopt}:s" } keys %{$format},
            )
        or pod2usage( %{$help->{error}} );

pod2usage( %{$help->{usage}}   ) if $opt->{usage};
pod2usage( %{$help->{full}}    ) if $opt->{help};
pod2usage( %{$help->{version}} ) if $opt->{version};

derive_default_options( $opt );

# get a tree of feature information to put into the index
my $feature_tree = scan_feature_dirs(
                                    topdir  => $opt->{dir},
                                    gherkin => gherkin->new(),
                                    );

my $rendered  = 0;
my $generated = 0;
foreach my $fmt ( sort keys %{$format} )
    {
    if( $opt->{$fmt} )
        {
        $opt->{verbose} and printf "generating %s index => %s ...\n", $fmt, $opt->{$fmt};
        my $g = $format->{$fmt}{generator}->new();
        my $t = $g->render( $feature_tree );
        $rendered++;
        if( open( my $fh, ">", $opt->{$fmt} ) )
            {
            print $fh $t;
            close $fh;
            $generated++;
            }
        else
            {
            printf STDERR "Error: failed to write to %s: %s\n", $opt->{$fmt}, $!;
            }
        }
    }

if( not $rendered )
    {
    printf STDERR "Error: Please specify at least 1 index format to generate\n";
    show_available_formats();
    exit 1;
    }

if( not $generated )
    {
    printf STDERR "Error: No index files were generated.\n";
    show_available_formats();
    exit 1;
    }

exit;

#-----------------------------------------------------------------------

sub derive_default_options
    {
    my $opt = shift;

    # --dir and --index are the only two independent options that can get
    # static defaults.
    $opt->{dir}   = 'features' if not $opt->{dir};
    $opt->{index} = 'index'    if not $opt->{index};

    # remove trailing /'s from topdir directory for cosmetic reasons
    $opt->{dir} =~ s{/*$}{};

    # if --index has a known suffix, make sure that that type of file will be generated.
    if( $opt->{index} =~ /(?<base>.+?)\.(?<suffix>\w+)$/ )
        {
        my $index_base   = $+{base};
        my $index_suffix = $+{suffix};
        SUFFIX_MATCH:
        foreach my $fmt ( keys %{$format} )
            {
            # re-use the suffix patterns as regex to match against known suffixes
            my $suffix_re = $format->{$fmt}{suffix};
            if( $index_suffix =~ /$suffix_re/ )
                {
                $opt->{$fmt}  //= $opt->{index};
                $opt->{index}   = $index_base;
                last SUFFIX_MATCH;
                }
            }
        }

    # setup the format => filename mappings for all requested formats
    foreach my $fmt ( keys %{$format} )
        {
        next if not defined $opt->{$fmt};
        # use the suffix patterns as regex to match against known suffixes
        my $suffix_re      = $format->{$fmt}{suffix};
        my $default_suffix = $format->{$fmt}{suffix} =~ s{\|.*}{}r;
        if( not $opt->{$fmt}                     ) { $opt->{$fmt}  = $opt->{index};                              }
        if( not $opt->{$fmt} =~ m:\.$suffix_re$: ) { $opt->{$fmt} .= sprintf ".%s",   $default_suffix;           }
        if( not $opt->{$fmt} =~ m:/:             ) { $opt->{$fmt}  = sprintf "%s/%s", $opt->{dir}, $opt->{$fmt}; }
        }

    return;
    }

sub show_available_formats
    {
    state $did_print = 0;
    if( not $did_print++ )
        {
        my $cmd = $0 =~ s{.*/}{}r;
        printf STDERR "       Available formats:\n";
        foreach my $fmt ( sort keys %{$format} )
            {
            my $suffix = $format->{$fmt}{suffix} =~ s/\|.*//r;
            foreach my $opt ( split( /\|/, $format->{$fmt}{getopt} ) )
                {
                printf STDERR "             %-10s %s --%s [<file>]\n", $fmt, $cmd, $opt;
                $fmt = '';
                }
            printf STDERR "                     or %s --index <file>.%s\n", $cmd, $suffix;
            }
        }
    }

#
# Search a directory, recursively, for '*.feature' files.
# Each directory or feature file is added to a tree of nodes which is then used
# to create the index.
#
# Each node in the feature tree has the stucture:
#
# Directories:
#   {
#   type        # 'dir'
#   name        # Human readable name of the directory or feature
#   contents    # array of files and/or directories in this dir
#   count       # number of feature files in the directory
#   parent      # link to parent object in the tree
#   }
#
# Feature Files:
#   {
#   type        # 'feature'
#   name        # Human readable name of the directory or feature
#   path        # Name of the file or directories path relative to the 'features/' directory
#   description # Array of descriptive text for the features
#   }
#
sub scan_feature_dirs
    {
    my $param        = { @_ };
    my $topdir       = $param->{topdir};
    my $gherkin      = $param->{gherkin};
    my $node         = {
                       type     => 'dir',
                       name     => '',
                       contents => [],
                       count    => 0,
                       parent   => undef,
                       };
    my $feature_tree = $node;
    my $find_opts    = {
                       no_chdir     => 1,   # don't chdir into each subdirectory - keep all paths relative to topdir
                       follow       => 0,   # must be 0 to use preprocess
                       follow_fast  => 0,   # must be 0 to use preprocess
                       preprocess   =>  sub {
                                            #
                                            # Capture the enclosing directory name as an index heading
                                            #
                                            my $leaf = $File::Find::dir =~ s{.*/}{}r;
                                            push @{$node->{contents}}, {
                                                                       type     => 'dir',
                                                                       name     => $leaf
                                                                                    =~ s{_+}{ }gr        # replace _'s with spaces
                                                                                    =~ s{\b\w}{\u$&}gr,  # uppercase each first character
                                                                       contents => [],
                                                                       count    => 0,
                                                                       parent   => $node,
                                                                       };
                                            $node = $node->{contents}[-1];
                                            # return list of files/directories we were given
                                            return @_;
                                            },
                       wanted       =>  sub {
                                            #
                                            # Parse all features files - ignore anything else
                                            #
                                            return if not $File::Find::name =~ /\.feature$/;

                                            my $file_name = $File::Find::name;
                                            if( open( my $file_handle, '<', $file_name ) )
                                                {
                                                # feature files should be small enough to squeeze into memory
                                                # we slurp the file here to simplify testing
                                                # i.e.: make it easier to provide parse_feature() with
                                                # fake files, and remove parse_feature()'s dependency on the
                                                # file system
                                                local $/;
                                                my $file_text = <$file_handle>;
                                                close $file_handle;

                                                # $feature is expected to have the structure:
                                                #
                                                #   {
                                                #   name        => ...  # human readable feature name
                                                #   path        => ...  # the relative path to the feature
                                                #   description => ...  # description of the feature
                                                #   }
                                                #
                                                if( my $feature = $gherkin->parse_feature( $file_name, $file_text ) )
                                                    {
                                                    # add the feature to this directories node list
                                                    push @{$node->{contents}}, {
                                                                               type        => 'feature',
                                                                               path        => $file_name =~ s{${topdir}/*}{}r,
                                                                               name        => $feature->{name},
                                                                               description => $feature->{description},
                                                                               };

                                                    # increase the 'feature count' of all parent directories
                                                    my $parent = $node;
                                                    while( $parent )
                                                        {
                                                        $parent->{count}++;
                                                        $parent = $parent->{parent};
                                                        }
                                                    }
                                                }
                                            else
                                                {
                                                warn sprintf "Failed to read '%s' - %s\n", $file_name, $!;
                                                }
                                            },
                       postprocess  =>  sub {
                                            #
                                            # Finish processing a directory
                                            #    - if it was empty => discard
                                            #    - otherwise, sort feature files by their feature names (not by filename!)
                                            #      and move sub-directories after all features in this directory
                                            #
                                            my $parent = $node->{parent};

                                            # discard empty directories
                                            pop @{$parent->{contents}} if not $node->{count};

                                            # sort contents by type (features, then sub-directories)
                                            # and the name being displayed in the index
                                            @{$node->{contents}} = map  { $_->[0] }
                                                                   sort {
                                                                            $a->[1] <=> $b->[1]
                                                                        or  $a->[2] cmp $b->[2]
                                                                        }
                                                                   map  {   [
                                                                            $_,                                 # item object
                                                                            $_->{type} eq 'feature' ? 0 : 1,    # 0: files, 1: directories
                                                                            fc( $_->{name} )                    # visible name
                                                                            ]
                                                                        }
                                                                   @{$node->{contents}};

                                            # remove scaffolding
                                            delete $node->{count};
                                            delete $node->{parent};

                                            # move up to parent directory
                                            $node = $parent;
                                            },
                       };

    find( $find_opts, ( $topdir ) );

    # remove the 'top nodes' that only have a single sub-directory in their contents
    while( @{$feature_tree->{contents}} == 1 and $feature_tree->{contents}[0]{type} eq 'dir' )
        {
        $feature_tree = $feature_tree->{contents}[0];
        }

    return $feature_tree;
    }

####################################################################################################
#
# HTML FORMATTER
#

package generator::html;

use strict;
use warnings;

sub new { return bless {}, shift; }

sub render
    {
    my $self         = shift;
    my $feature_tree = shift;  # an AST as generated by scan_feature_dirs()

    my $index_html   = $self->feature_tree_as_html( $feature_tree );

    # set up the dynamic contents to be inserted into the HTML template
    my $html_var     = {
                       feature_index => join( "\n", @{$index_html} ),
                       };

    my $html = $self->html_template();
    $html =~ s{
              {(?<name>\w+)}                # replace placeholders in the html template...
              }
              {
              exists $html_var->{$+{name}}
                  ? $html_var->{$+{name}}   # with their defined values
                  : $&                      # or leave them unchanged
              }egx;

    return $html;
    }

sub feature_tree_as_html
    {
    my $self  = shift;
    my $node  = shift;
    my $html  = shift || [];

    if( $node->{type} eq 'dir' )
        {
        push @{$html}, '<div class=dir>';
        push @{$html}, sprintf "<label>%s</label>", $node->{name}    if $node->{name};
        push @{$html}, '<ul>';
        foreach my $child ( @{$node->{contents}} )
            {
            push @{$html}, '<li>';
            $self->feature_tree_as_html( $child, $html ); # recursion - yay!
            push @{$html}, '</li>';
            }
        push @{$html}, '</ul>';
        push @{$html}, '</div>';
        }
    else # only one other option: feature!
        {
        push @{$html}, '<div class=feature>';
        push @{$html}, '<label>';
        push @{$html}, sprintf "<a href=\"%s\">%s</a>", $node->{path}, $node->{name};
        push @{$html}, '</label>';
        if( $node->{description} )
            {
            push @{$html}, '<pre class=description><code>';
            push @{$html}, $node->{description};
            push @{$html}, '</code></pre>';
            }
        else
            {
            push @{$html}, '<div class="empty">no description</div>';
            }
        push @{$html}, '</div>';
        }

    return $html;
    }

sub html_template
    {
    return <<_EO_TEMPLATE_;
<!doctype html>
<html>
<head>
    <title> Features </title>
    <meta charset = "utf-8">

    <style>

        ul                              {
                                        list-style:     none;
                                        border-left:    1px solid #ddd;
                                        padding-left:   2em;
                                        }

        .dir label                      {
                                        font-weight:    bold;
                                        line-height:     2em;
                                        }

        .feature label                  {
                                        font-weight:    bold;
                                        }

        .feature .description           {
                                        display:        none;
                                        padding:        0 1em 1em;  /* top, right, bottom( , left ) - clockwise, copy missing from opposite side */
                                        border:         1px solid blue;
                                        border-radius:  1ch;
                                        background:     #eef;
                                        }

        .feature .empty                 {
                                        display:        none;
                                        padding:        1em;
                                        border:         1px solid #aaa;
                                        border-radius:  1ch;
                                        background:     #fafafa;
                                        color:          #888;
                                        font-style:     italic;
                                        }

        .feature:hover .description,
        .feature:hover .empty           {
                                        display:        block;
                                        }

    </style>

</head>

<body>

    <main>

        <div id=index>
            {feature_index}
        </div>

    </main>

</body>

</html>
_EO_TEMPLATE_
    }

####################################################################################################
#
# MARKDOWN FORMATTER
#
# References:
#
#       https://daringfireball.net/projects/markdown/
#       https://tools.ietf.org/html/rfc7763
#       https://tools.ietf.org/html/rfc7764
#       https://help.github.com/articles/github-flavored-markdown/
#       https://github.com/github/markup/tree/master#html-sanitization
#

package generator::markdown;

use strict;
use warnings;

sub new { return bless {}, shift; }

sub render
    {
    my $self            = shift;
    my $feature_tree    = shift;  # an AST as generated by scan_feature_dirs()

    my $markdown        = $self->feature_tree_as_markdown( $feature_tree );

    return join( "\n", @{$markdown} );
    }

sub feature_tree_as_markdown
    {
    my $self     = shift;
    my $node     = shift;
    my $markdown = shift || [];
    my $depth    = ( shift || 0 ) + 1;

    if( $node->{type} eq 'dir' )
        {
        push @{$markdown}, sprintf "%s %s", '#' x $depth, $node->{name};
        push @{$markdown}, '';
        foreach my $child ( @{$node->{contents}} )
            {
            $self->feature_tree_as_markdown( $child, $markdown, $depth ); # recursion - yay!
            }
        }
    else # only one other option: feature!
        {
        push @{$markdown}, sprintf "* [%s](%s)", $node->{name}, $node->{path};
        push @{$markdown}, '';
        if( $node->{description} )
            {
            # TODO: 2020-11-17 Problem with markdown: anything indented > 4 spaces is considered 'code'
            # The descriptions only have their relative indentation - which can
            # give us surprising combinations of 'text' and 'code'.
            # Alternative: remove all indentation?
            #
            # plain text => strange indentation
            # push @{$markdown}, $node->{description};
            #
            # common indent => bizarre 'hash' errors - but only via Markdown.pl (John Gruber, 2004)
            # push @{$markdown}, map { s{^\s*}{  }r } $node->{description};
            #
            # ensure that all lines with text are indented 2 spaces, to line up
            # with the feature name, while keeping empty lines empty
            push @{$markdown}, $node->{description} =~ s/^\s*/  /mgr;
            }
        else
            {
            push @{$markdown}, '  _no description_';
            }
        push @{$markdown}, '';
        }

    return $markdown;
    }

####################################################################################################
#
# YAML FORMATTER
#
# Note: the YAML module is NOT in perl's core distribution
# For this purpose, we don't really need it, just a simple recursive structure
#

package generator::yaml;

use strict;
use warnings;

sub new { return bless {}, shift; }

sub render
    {
    my $self         = shift;
    my $feature_tree = shift;  # an AST as generated by scan_feature_dirs()

    my $yaml         = $self->feature_tree_as_yaml( $feature_tree );

    return sprintf "---\n%s\n", join( "\n", @{$yaml} );
    }

sub feature_tree_as_yaml
    {
    my $self   = shift;
    my $node   = shift;
    my $yaml   = shift || [];
    my $depth  = ( shift || 0 ) + 1;

    my $indent = '    ' x $depth;

    if( $node->{type} eq 'dir' )
        {
        push @{$yaml}, sprintf "%s- name: %s", $indent, yaml_string( $node->{name} );
        push @{$yaml}, sprintf "%s  contents:", $indent;
        foreach my $child ( @{$node->{contents}} )
            {
            $self->feature_tree_as_yaml( $child, $yaml, $depth ); # recursion - yay!
            }
        }
    else # only one other option: feature!
        {
        push @{$yaml}, sprintf "%s- name: %s", $indent, yaml_string( $node->{name} );
        push @{$yaml}, sprintf "%s  path: %s", $indent, yaml_string( $node->{path} );
        if( $node->{description} )
            {
            push @{$yaml}, sprintf "%s  description:", $indent;
            foreach my $line ( $node->{description} )
                {
                push @{$yaml}, sprintf "%s      - %s", $indent, yaml_string( $line );
                }
            }
        else
            {
            push @{$yaml}, sprintf "%s  description: []", $indent;
            }
        }

    return $yaml;
    }

sub yaml_string
    {
    my $string = shift;
    return $string =~ /^\s|\s$|^$/
            ? $string =~ /'/
                ? sprintf( "\"%s\"", $string =~ s/"/\\"/gr )
                : sprintf( "\'%s\'", $string )
            : $string;
    }

####################################################################################################
#
# JSON FORMATTER
#
# Note: the JSON module is NOT in perl's core distribution
# For this purpose, we don't really need it, just a simple recursive structure
#

package generator::json;

use strict;
use warnings;

sub new { return bless {}, shift; }

sub render
    {
    my $self         = shift;
    my $feature_tree = shift;  # an AST as generated by scan_feature_dirs()

    my $json = $self->feature_tree_as_json( $feature_tree );
    $json->[-1] =~ s/,$//;

    return sprintf "%s\n", join( "\n", @{$json} );
    }

sub feature_tree_as_json
    {
    my $self   = shift;
    my $node   = shift;
    my $json   = shift || [];
    my $depth  = ( shift || 0 ) + 1;

    my $indent = '  ' x ( $depth - 1 );

    if( $node->{type} eq 'dir' )
        {
        push @{$json}, sprintf "%s{",                 $indent;
        push @{$json}, sprintf "%s\"name\":     %s,", $indent, json_string( $node->{name} );
        push @{$json}, sprintf "%s\"contents\": [",   $indent;
        foreach my $child ( @{$node->{contents}} )
            {
            $self->feature_tree_as_json( $child, $json, $depth + 6 ); # recursion - yay!
            }
        $json->[-1] =~ s/,$//;
        push @{$json}, sprintf "%s          ]", $indent;
        push @{$json}, sprintf "%s},",          $indent;
        }
    else # only one other option: feature!
        {
        push @{$json}, sprintf "%s{",             $indent;
        push @{$json}, sprintf "%s\"name\": %s,", $indent, json_string( $node->{name} );
        push @{$json}, sprintf "%s\"path\": %s,", $indent, json_string( $node->{path} );
        if( $node->{description} )
            {
            push @{$json}, sprintf "%s\"description\": [", $indent;
            foreach my $line ( split( /\v/, $node->{description} ) )
                {
                push @{$json}, sprintf "%s      %s,", $indent, json_string( $line );
                }
            $json->[-1] =~ s/,$//;
            push @{$json}, sprintf "%s      ]", $indent;
            }
        else
            {
            push @{$json}, sprintf "%s\"description\": []", $indent;
            }
        push @{$json}, sprintf "%s},", $indent;
        }

    return $json;
    }

sub json_string
    {
    my $string = shift;
    return sprintf( "\"%s\"", $string =~ s/"/\\"/gr )
    }

####################################################################################################
#
# The 'gherkin' package acts as a cache of known languages
#
package gherkin;

use strict;
use warnings;

use Text::Tabs qw( expand );    # core module since perl v5

sub new
    {
    my $class     = shift;
    my $languages = shift;  # optional languages JSON for testing

    my $self = bless {}, $class;
    $self->{languages} = gherkin::languages->new( $languages );

    # parsers are created lazily, as needed
    $self->{parser} = {};

    return $self;
    }

sub parse_feature
    {
    my $self      = shift;
    my $file_name = shift;
    my $file_text = shift or return;

    #
    # Steps:
    #   1. identify the locale used in the feature file
    #   2. load gherkin parser for the file's locale
    #   3. parse each line individually
    #       3a. extract feature name
    #       3b. extract feature description
    #       3c. stop at first keyword after initial description.
    #

    # feature tree node - see the description in scan_feature_dirs()'s comments
    my $info = {
               name        => undef,
               description => undef,
               };

    # 1. identify feature file language
    my ( $locale ) = ( $file_text =~ /^\s*#\s*language:\s*(\S+)/ );

    # 2. load language parser (regex, optionally derived from gherkin-languages.json)
    my $parser = $self->parser_for_locale( $locale );
    $info->{locale} = $parser->{locale};

    my $ignore_line_re  = $parser->ignore_line_re();
    my $keyword_line_re = $parser->keyword_line_re();

    # 3. parse each line individually
    my $section_num = 0;
    my $text_lines = [];
    foreach my $line ( split( /\v/, $file_text ) )
        {
        next            if $line        =~ $ignore_line_re;   # skip all comments and tag lines
        $section_num++  if $line        =~ $keyword_line_re;  # have we started a new section?
        next            if $section_num == 0;                 # skip everything until we at least get to the first section
        last            if $section_num  > 1;                 # stop after finishing the 1st section

        my $name = $+{name};    # grab name from keyword match above (most recent regex)

        if( $name )
            {
            # The first line of the first section will be 'Feature:',
            # 'Ability:', 'Business Need:' or any other language specific synonym.
            $info->{name} = ucfirst( $name =~ s{\s+}{ }gr ); # squash tabs and multiple spaces in the name
            }
        else
            {
            push @{$text_lines}, $line;
            }
        }

    # expand tabs and merge all text lines lines into a single scalar
    my $text = join( "\n", expand( @{$text_lines} ) );

    # determine minimum indent via regex
    my ( $indent ) = sort { $a <=> $b } map { length } ( $text =~ /^(\h*)\S/mg ); 

    # remove unwanted lines, spaces etc.
    $text =~ s/^\s*//m;                     # remove leading blank lines
    $text =~ s/\s*\z/\n/m;                  # remove trailing blank lines (but keep the last one)
    $text =~ s/(\v\s*){2,}\v/\n\n/mg;       # remove multiple empty lines
    $text =~ s/^\h{$indent}//mg if $indent; # remove indents

    $info->{description} = $text;

    return $info;
    }

sub parser_for_locale
    {
    my $self   = shift;
    my $locale = lc( shift // 'en' );

    return $self->{parser}{$locale}
       //= gherkin::parser->new( $locale, $self->{languages}->gherkin_for_locale( $locale ) );
    }

#
# Regex-based Gherkin parser
#
# This is not supposed to be a real parser, just enough to get the feature and
# description of each feature.
#
# All this does is use a language specification as defined in
# gherkin-languages.json and provde some simple regular expressions for finding
# a feature's name and description.
#
# Note: the keyword_line_re MUST defined named capture groups:
#   $+{keyword}     the actual keyword that was matched
#   $+{name}        the name of the section started by the keyword
#
package gherkin::parser;

use strict;
use warnings;

sub new
    {
    my $class    = shift;
    my $locale   = shift; # short language identifier, e.g.: 'en', 'en-au' or 'de', ...
    my $language = shift; # per-language structure provided by gherkin-languages.json

    # turn the known keywords for this language into a simple a|b|c|... pattern
    # which can be captured via $self->{keyword_line_re}
    my $keywords_pattern     = join( '|',
                                        map { @{$language->{$_}} }
                                        qw( feature rule background scenario scenarioOutline )
                                   ) =~ s{\s+}{\\s+}gr;

    my $self = bless {}, $class;
    $self->{locale}          = $locale;
    $self->{ignore_line_re}  = qr/^\s*[#@]/; # the same for all languages (so far)
    $self->{keyword_line_re} = qr/^\s*(?<keyword>$keywords_pattern)\s*:\s*(?<name>.*)/;

    return $self;
    }

sub ignore_line_re  { my $self = shift; return $self->{ignore_line_re};  }
sub keyword_line_re { my $self = shift; return $self->{keyword_line_re}; }

# IMPORTANT: this MUST be the last module in this script
#            so that <DATA> accesses the gherkin-languages.json appended
#            to the end of this script by build_gfindex.PL
package gherkin::languages;

use strict;
use warnings;

use Carp     qw( croak );       # core module since perl v5
use JSON::PP qw( decode_json ); # core module since perl v5.13.9

# keep a single, global copy of <DATA>'s json because <DATA> can't be read
# twice (easily)
my $data_languages;

sub new
    {
    my $class     = shift;
    my $languages = shift;  # optional languages JSON for testing

    local $/;   # slurp all of <DATA> as a single scalar
    my $self = decode_json( $languages || ( $data_languages //= <DATA> ) );

    return bless $self, $class;
    }

sub gherkin_for_locale
    {
    my $self   = shift;
    my $locale = shift;

    croak "Unknown Gherkin language '%s'\n", $locale if not $self->{$locale};

    return $self->{$locale}
    }

# gherkin-languages.json
# ----------------------
# <appended by build_gfindex.PL> ...
