Gherkin Feature Indexer
=======================

This project attempts to bring order to chaotic collections of feature files
for a project being built with Behaviour Driven Design (BDD) techniques using
[Gherkin][1].

The key element of any BDD project using [Gherkin][1] is the `features/`
directory. Down there you'll find many files with the `.feature` suffix, which
describe the acceptance criteria that the project is expected to fulfill.

Feature files are written in a very simple language, [Gherkin][1] (developed
by the guys who also developed the [Cucumber BDD testing framework][2]).

The only problem with the `features/` directory is that it has no obvious
structure. All of the features may be dumped into a single directory, or they
may be divided into a deep directory structure.

In both cases, it can be difficult to find the feature you're looking for and
there is no logical structure of *chapters*, links between features etc.

`Gherkin Feature Indexer` (`gfindex`) scans all of your project's feature files
and builds an index in any of several formats.

[1]: https://cucumber.io/docs/gherkin/reference
[2]: https://cucumber.io

Installation:
-------------

Classic system-wide installation:
(e.g. `/usr/local/bin`, depending on your perl configuration)

    perl Makefile.PL
    make
    make test
    make install

Alternatively, after `make`, the following files can be copied into your
project:

    build/gfindex
    examples/git-pre-commit.sh

Typically, `gfindex` should be copied to a directory which is available to all
members or your project (e.g. in a `utils/` or `bin/` directory within your
project).

You can also make `gfindex` part of your build process by using a (pre-)commit
hook (via git-pre-commit.sh, see below).

The `gfindex` script is intended to be used autonomously. In other words, once
`make` (or `perl ./build_gfindex.PL`) has been run, the resulting `gfindex`
script in `blib/script/gfindex` contains all modules and data that it needs to
run and should work in any perl environment without the installation of any
further dependencies.

Localisation:
-------------

Gherkin can be written in a large number of languages.

As part of the build process, the latest version of gherkin-languages.json will
be downloaded and incorporated into the `gfindex` script.

For more details about localisation see `https://cucumber.io/docs/gherkin/languages/`

Git Hook:
---------

The `examples/git-pre-commit.sh` script shows how `gfindex` *could* be used
as a pre-commit hook to ensure that the features index is always
up-to-date.

Warning: check that you don't already have a pre-commit hook - if so you will
have to do some script hacking

    project-root/ % cp examples/git-pre-commit.sh {your project}/support/

edit support/git-pre-commit.sh
set the path to gfindex (relative to your project's workspace)
and check that the correct features and index file are configured.

Now, all project members can automatically update feature index by linking
the hook into their git repo's configuration:

    project-root/ % ln -s ../../support/git-pre-commit.sh {your project}/.git/hooks/pre-commit

Note: `ln -s` simply creates a symlink containing the first argument, without
translating the path - so the above command works relative to `.git/hooks`,
regardless of the current working directory.

Usage:
------

`gfindex` assumes that your current directory is the top of your
project workspace. By default, it expects to find a directory called
`features/` directly in your current working directory.

    % perl gfindex --md

or, if gfindex is in your PATH

    % gfindex --md

Options:
--------

- `--dir   <dir>        the features/ directory to be scanned`
- `--md   [<file>]      generate a markdown index`
- `--html [<file>]      generate a html index`

Requirements:
-------------

* A Unix-like operating system (MacOS, Linux, *BSD, ...)
* Perl 5.18 or later (most OS's should have this pre-installed these days (2020...))

Testing:
--------

    % make test

References:
-----------

* [Gherkin Language Reference](https://cucumber.io/docs/gherkin/reference/)
* [Cucumber](https://cucumber.io)

-----------------------------
Copyright 2020-2021, Stephen Riehm
