#!/usr/bin/env perl

use v5.18;
use strict;
use warnings;

use ExtUtils::MakeMaker;

WriteMakefile(
    NAME             => 'Gherkin::FeatureIndexer',
    VERSION          => v0.0.4,
    AUTHOR           => 'Stephen Riehm <japh-codeberg\@opensauce.de>',
    LICENSE          => 'bsd',
    MIN_PERL_VERSION => v5.18,
    PL_FILES         => { 'build_gfindex.PL' => 'build/gfindex'       }, 
    MAN1PODS         => { 'build/gfindex'    => 'blib/man1/gfindex.1' },
    EXE_FILES        => [ 'build/gfindex' ], # generated by build_gfindex.PL
                        
    # target specific configurations
    test             => { TESTS => 'test/*.t' },
    clean            => { FILES => 'build'    },
);
